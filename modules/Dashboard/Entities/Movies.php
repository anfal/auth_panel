<?php

namespace modules\Dashboard\Entities;

use Illuminate\Database\Eloquent\Model;

class Movies extends Model
{
    protected $table = "movies";
}
