/**
 * Created by anfal on 12/22/16.
 */

var dashboard = {
    url: {
        fetch_movies_url: ''
    },

    fetch_movies: function(user_id) {
        var final_url = dashboard.url.fetch_movies_url + "/" + user_id;
        utility.send_ajax_request(
            final_url,
            null,
            utility.request_type.GET,
            dashboard.before_fetching_movies,
            dashboard.fetch_movies_success,
            dashboard.fetch_movies_failure
        );
    },

    fetch_movies_success: function(data) {
        var data_div = $('#data_div');
        var movies = JSON.parse(data);
        data_div.empty();
        data_div.append(dashboard.get_movie_list_table(movies.movie_list));
    },

    fetch_movies_failure: function (data) {

    },

    before_fetching_movies: function () {
        var data_div = $('#data_div');
        data_div.empty();
        data_div.append('Loading...');
    },

    get_movie_list_table: function (movie_list) {
        var movies = movie_list.map(function(movie) {
            return "<tr>"+
                    "<td>" +
                        movie +
                    "</td>" +
                "</tr>";
        });

        return "<table class='table'>" +
                "<thead>" +
                    "<th>Movies</th>" +
                "</thead>" +
                "<tbody>" +
                    movies +
                "</tbody>" +
            "</table>";
    }
};