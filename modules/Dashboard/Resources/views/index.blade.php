@extends('layouts.app')

@push('child_styles')
    <link rel="stylesheet" href="{!! Module::asset('dashboard:css/dashboard.css') !!}"/>
@endpush

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading dashboard_panel_heading">
                    <button
                            type="button"
                            class="btn btn-success"
                            id="fetch_movies_button"
                            onclick="dashboard.fetch_movies({!! Auth::id() !!})"
                    >
                        {!! trans('generic.show_movies') !!}
                    </button>
                </div>
                <div class="panel-body">
                    <div id="data_div"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('child_scripts')
    <script type="text/javascript" src="{!! Module::asset('dashboard:js/dashboard.js') !!}"></script>
@endpush

@section('in_html_scripts')
    <script type="text/javascript">
        dashboard.url.fetch_movies_url = "{!! url('/dashboard/fetch_movies') !!}";
    </script>
@stop