<?php

Route::group(['middleware' => 'web', 'prefix' => 'dashboard', 'namespace' => 'modules\Dashboard\Http\Controllers'], function()
{
    Route::get('/', 'DashboardController@index');
    Route::get('/fetch_movies/{user_id}', 'MoviesController@fetch_movies');
});
