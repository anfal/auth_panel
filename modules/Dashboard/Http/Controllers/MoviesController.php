<?php

namespace modules\Dashboard\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use modules\Dashboard\Entities\Movies;

class MoviesController extends Controller
{
    public function fetch_movies($user_id)
    {
        $movies = Movies::where('user_id', $user_id)->get();
        $movie_list = [];
        foreach ($movies as $movie) {
            $movie_list[] = $movie->movie_name;
        }
        return json_encode(['movie_list' => $movie_list]);
    }

}
