@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @include('individual_components.panel_with_heading', [
                'panel_heading' => trans('generic.heading'),
                'panel_body' => trans('generic.welcome')
            ])
        </div>
    </div>
</div>
@endsection