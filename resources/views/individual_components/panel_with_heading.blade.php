@push('child_styles')
    <link rel="stylesheet" href="{!! asset('css/panel_with_heading.css') !!}"/>
@endpush

<div class="panel panel-default">
    <div class="panel-heading pwh_heading">{!! $panel_heading !!}</div>

    <div class="panel-body">
        {!! $panel_body !!}
    </div>
</div>