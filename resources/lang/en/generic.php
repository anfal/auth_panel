<?php
/**
 * Created by PhpStorm.
 * User: anfal
 * Date: 12/22/16
 * Time: 10:34 AM
 */

return [

    'heading' => 'This is a Heading.',
    'welcome' => 'Welcome this is a practice app, please proceed to the authentication.',
    'home' => 'Home',
    'login' => 'Login',
    'register' => 'Register',
    'email_address' => 'E-Mail Address',
    'password' => 'Password',
    'remember_me' => 'Remember me',
    'forgot_password_question' => 'Forgot your password?',
    'name' => 'Name',
    'confirm_password' => 'Confirm Password',
    'go_back' => 'Go Back?',
    'nothing_to_see_here' => 'Nothing to see here.',
    'show_movies' => 'Show movies'
];
