/**
 * Created by anfal on 12/22/16.
 */

var utility = {

    request_type: {
        GET: 'GET'
    },

    send_ajax_request: function(
        url,
        data,
        request_type,
        before_send_callback,
        success_callback,
        error_callback
    ) {
        switch(request_type) {
            case this.request_type.GET:
                $.ajax({
                    url: url,
                    method: request_type,
                    beforeSend: function () {
                        before_send_callback();
                    },
                    success: function(data) {
                        success_callback(data);
                    },
                    error: function(data) {
                        error_callback(data);
                    }
                });
        }
    }
};